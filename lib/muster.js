'use strict'

const express = require('express')
const vhost = require('vhost')
const vasync = require('vasync')
const crypto = require('crypto')
const cluster = require('cluster')
const os = require('os')

const DEFAULT_LOADBALANCER = 'shared'
const DEFAULT_GRACE_PERIOD = 10000
const DEFAULT_THREAD_COUNT = os.cpus().length
const DEFAULT_PORT = 8080
const DEFAULT_REQUEST_LIMIT = 10000
const DEAFULT_MEMORY_LIMIT = 750

const debug = require('debug')('muster')
const log = require('./logger')

const { workerMiddleware, fallbackError, fallbackProduction, fallbackDevelopment } = require('./middleware')

/**
 * @class Muster
 * a packaged clustered webserver
 */
class Muster {
  /**
   * @constructor initiate server instance
   * @param {Object} options - configuration options for the server
   */
  constructor (options) {
    this.server = express()
    this.virtualhosts = []
    this.middleware = []
    this.env = process.env.NODE_ENV = process.env.NODE_ENV || 'development'
    this.delegateOptions(options)
  }

  /** @method launch build and deploy the server */
  launch () {
    const applyMiddleware = (middleware) => {
      if (Array.isArray(middleware)) {
        this.server.use(...middleware)
      } else {
        this.server.use(middleware)
      }
    }

    const build = async () => {
      let self = this
      // TODO make foreach pipeline instead
      return new Promise((resolve, reject) => {
        vasync.pipeline({
          'funcs': [
            function buildApplyMiddleware (_, callback) {
              applyMiddleware(self.middleware)
              callback()
            },
            function buildWorkerMiddleware (_, callback) {
              applyMiddleware(workerMiddleware)
              callback()
            },
            function buildApplyVirtualHosts (_, callback) {
              applyMiddleware(self.virtualhosts)
              callback()
            },
            function buildFallbackRouter (_, callback) {
              applyMiddleware(fallbackError)
              if (self.env !== 'production') {
                applyMiddleware(fallbackDevelopment)
              } else {
                applyMiddleware(fallbackProduction)
              }
              callback()
            }
          ]
        }, (err) => {
          if (err) {
            reject(err)
          } else {
            resolve({
              env: self.env,
              server: self.server,
              options: self.options
            })
          }
        })
      })
    }

    const startServer = (env) => {
      let sel
      const start = {
        production: () => {
          const key = crypto.randomBytes(256).toString('hex')
          build()
            .then((build) => {
              if (cluster.isMaster) {
                debug(`starting in Production\n${JSON.stringify(this.options, null, 2)}`)
                const masterProcess = new Master()
                masterProcess.define(Object.assign({ key: key }, build))
                masterProcess.fork()
              } else {
                const workerProcess = new Worker()
                workerProcess.define(Object.assign({ key: key }, build))
                workerProcess.listen()
              }
            })
            .catch((err) => debug(`build error: ${err}`))
        },
        development: () => {
          process.env.key = crypto.randomBytes(256).toString('hex')
          build()
            .then((build) => {
              debug(`starting in Development`)
              build.server.listen(this.options.port, () => debug(`[Development] listening on: ${this.options.port}`))
            })
            .catch((err) => debug(`build error: ${err}`))
        },
        invalid: () => {
          log.info(`invalid NODE_ENV: ${this.env}, assign to either 'development' or 'production'`)
          process.exit(1)
        }
      }

      if (start[env]) {
        sel = start[env]
      } else {
        sel = start['invalid']
      }
      return sel()
    }
    startServer(this.env)
  }

  /**
   * @method module register an virtual host
   * @param {String} hostname - hostname to be associated 'www.example.com'
   * @param {Object} module - an express() instance
   */
  module (hostname, module) {
    this.virtualhosts.push(vhost(hostname, module))
  }

  /**
   * @method use register an express middleware function
   * @param {Function} middleware - middleware function to be incorporated with the app
   */
  use (middleware) {
    this.middleware.push(middleware)
  }

  /**
   * @method delegateOptions define which options will be used
   * @param {Object} options - object of given options
   */
  delegateOptions (options) {
    this.options = options || {}
    this.loadbalancer = this.options.loadbalancer = this.options.loadbalancer || DEFAULT_LOADBALANCER
    this.grace = this.options.grace = this.options.grace || DEFAULT_GRACE_PERIOD
    this.threads = this.options.threads = this.options.threads || DEFAULT_THREAD_COUNT
    this.port = this.options.port = this.options.port || DEFAULT_PORT
    this.requestLimit = this.options.requestLimit = this.options.requestLimit || DEFAULT_REQUEST_LIMIT
    this.memoryLimit = this.options.memoryLimit = this.options.memoryLimit || DEAFULT_MEMORY_LIMIT
  }
}

/**
 * @class Master
 * represents the master process for the application
 */
class Master {
  /** @constructor */
  constructor () {
    // bind functions (why is this needed?)
    // TODO learn javascript
    this.mapWorker = this.mapWorker.bind(this)
    this.getMappedWorker = this.getMappedWorker.bind(this)
    this.removeMappedWorker = this.removeMappedWorker.bind(this)
    this.define = this.define.bind(this)
    this.fork = this.fork.bind(this)
    this.spawn = this.spawn.bind(this)
    this.respawn = this.respawn.bind(this)
    this.kill = this.kill.bind(this)
    this.shutdown = this.shutdown.bind(this)
    this.handler = this.handler.bind(this)
    this.listen = this.listen.bind(this)
  }

  /**
   * @method mapWorker map workers to a thread id
   * @param {Number} cid - cluster id from the cluster object
   * @param {Number} id - the thread id
   */
  mapWorker (cid, id) {
    this.workerMap[cid] = id
  }

  /**
   * @method getMappedWorker return the thread id from the worker map
   * @param {Number} cid - cluster id from the cluster object
   * @returns {Number} the thread id
   */
  getMappedWorker (cid) {
    return Number(this.workerMap[cid])
  }

  /**
   * @method removeMappedWorker remove the thread id from the worker map
   * @param {Number} cid - cluster id from the cluster object
   */
  removeMappedWorker (cid) {
    delete this.workerMap[cid]
  }

  /**
   * @method define initiates a Master object ready to delegate workers
   * @param {Object} build - a build object with the options and key
   */
  define (build) {
    this.key = build.key
    this.env = build.env
    this.server = build.server
    this.options = build.options
    this.running = true
    this.workerMap = {}
    this.tag = `[Master 0:${process.pid}]`

    debug(`${this.tag} process has started`)
    this.listen()
  }

  /** @method fork spawns processes based on the given thread count and associate thread ids based on the loadbalancer */
  fork () {
    if (this.env !== 'production') {
      // in development mode the master process will just spawn one process
      this.spawn(0)
    } else {
      if (this.options.loadbalancer === 'split') {
        // this is the thread 'id' that will be assinged to each worker
        // this number will remain consistient throughout the life of
        // the process
        let id = 1
        for (let i = 0; i < this.options.threads; i++) {
          this.spawn(id++)
        }
      } else {
        let id = 0
        for (let i = 0; i < this.options.threads; i++) {
          this.spawn(id)
        }
      }
    }
  }

  /**
   * @method spawn fork a single process
   * @param {Number} id - thread id to be associated to the process
   * @returns {Object} worker - the clustered worker object
   */
  spawn (id) {
    // the thread id, session key and worker limits are passed through
    // to the workers process.env
    const worker = cluster.fork({
      id: id,
      key: this.key,
      memoryLimit: this.memoryLimit,
      requestLimit: this.requestLimit
    })
    this.mapWorker(worker.id, id)
    // send all messages to the handler
    worker.on('message', this.handler)
    return worker
  }

  /**
   * @method respawn in the event a worker exits repawn the worker with the same thread id
   * @param {Object} worker - the clustered worker object
   * @param {Number} code - the error code given
   * @param {Number} signal - the exit signal recieved by the message
   */
  respawn (worker, code, signal) {
    // only respawn if the application is still running
    if (this.running && !worker.exitedAfterDisconnect) {
      let id = this.getMappedWorker(worker.id)
      debug(`${this.tag} [Worker ${id}][${worker.id}:${worker.process.pid}] exited, respawning`)
      this.spawn(id)
      this.removeMappedWorker(worker)
    }
  }

  /**
   * @method kill kills the worker process and confirms the kill after a grace period
   * @param {Object} worker - the clustered worker object
   */
  kill (worker) {
    worker.process.kill()
    // confirm worker is dead after the grace period
    setTimeout(() => {
      worker.kill()
    }, this.options.grace).unref()
    worker.disconnect()
  }

  /** @method shutdown when a shutdown is trigger each active worker is killed */
  shutdown () {
    debug(`${this.tag} shutting down`)
    this.running = false
    for (let id in cluster.workers) {
      let worker = cluster.workers[id]
      let cid = worker.id
      let pid = worker.process.pid
      let _id = this.getMappedWorker(cid)
      debug(`${this.tag} killing [Worker ${_id}][${cid}:${pid}]`)
      this.kill(worker)
    }
  }

  /**
   * @method handler deals with worker messages
   * @param {Object} msg - the message sent from a worker
   */
  handler (msg) {
    if (this.running && msg.cmd && msg.cmd === 'kill') {
      let worker = cluster.workers[msg.worker]
      let id = this.getMappedWorker(worker.id)
      let tag = `[Worker ${id}][${worker.id}:${worker.process.pid}]`
      if (worker) {
        debug(`${this.tag} killing ${tag}: ${msg.reason}`)
        const replacement = this.spawn(id)
        replacement.on('listening', (address) => {
          debug(`${this.tag} replacing ${tag} with [${replacement.id}:${replacement.process.pid}]`)
          this.kill(worker)
        })
      }
    }
  }

  /** @method listen initiates event listeners */
  listen () {
    process.on('SIGINT', this.shutdown)
    process.on('SIGTERM', this.shutdown)
    cluster.on('exit', this.respawn)
  }
}

/**
 * @class Worker
 * a worker process instance
 */
class Worker {
  /** @constructor */
  constructor () {
    this.define = this.define.bind(this)
    this.listen = this.listen.bind(this)
  }

  /**
   * @method define initiate the build to be used by the worker process
   * @param {Object} build - a bundled build object with the id of the worker and the express instance
   */
  define (build) {
    this.server = build.server
    this.id = this.server.locals.id = Number(process.env.id)
    this.cid = this.server.locals.cid = cluster.worker.id
    this.pid = this.server.locals.pid = cluster.worker.process.pid
    this.server.locals.memoryLimit = process.env.memoryLimit
    this.server.locals.requestLimit = process.env.requestLimit
    this.port = build.options.port + this.id
    this.tag = `[Worker ${this.id}][${this.cid}:${this.pid}]`
  }

  /** @method listen starts the server */
  listen () {
    // TODO https aswell with option
    this.server.listen(this.port, () => {
      debug(`${this.tag} listening on: ${this.port}`)
    })
  }
}

module.exports = Muster
