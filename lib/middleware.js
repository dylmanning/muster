'use strict'

const middlewareController = {

  workerMiddleware: (req, res, next) => {
    let id = req.app.locals.id
    let pid = req.app.locals.pid
    let cid = req.app.locals.cid
    let memoryLimit = req.app.locals.memoryLimit
    let requestLimit = req.app.locals.requestLimit--

    const killWorker = (reason) => {
      process.send({
        cmd: 'kill',
        id: id,
        pid: pid,
        cid: cid,
        reason: reason
      })
    }

    res.on('finish', () => {
      let memoryUsage = process.memoryUsage().rss / (1024 * 1024)

      if (memoryUsage >= memoryLimit) {
        killWorker(`Memory footprint limit of ${memoryLimit} reached`)
      }
      if (requestLimit-- === 0) {
        killWorker(`Request limit reached`)
      }
    })

    next()
  },

  fallbackError: (req, res, next) => {
    const err = new Error('Not Found')
    err.status = 404
    next(err)
  },

  fallbackProduction: (err, req, res, next) => {
    res.status(err.status || 500).json({
      'errors': {
        status: err.status,
        message: err.message,
        error: {}
      }
    })
  },

  fallbackDevelopment: (err, req, res, next) => {
    res.status(err.status || 500).json({
      'errors': {
        message: err.message,
        error: err
      }
    })
  }
}

module.exports = middlewareController
