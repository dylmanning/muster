'use strict'

const { createLogger, format, transports, config } = require('winston')

// TODO save logs to file

const loggerConfig = {
  label: 'muster',
  level: ((process.env.NODE_ENV !== 'development') ? 'info' : 'debug'),
  levels: {
    error: 0,
    warn: 1,
    info: 2,
    debug: 3
  },
  colors: {
    error: 'red',
    warn: 'yellow',
    info: 'blue',
    debug: 'green'
  }
}

// const consoleFormat = format.combine(
//   format.label({ label: loggerConfig.label }),
//   format.colorize(),
//   format.timestamp({
//     format: 'dddd Do MMMM, YYYY HH:mm:ss'
//   }),
//   format.printf(info => `${info.level} ${info.timestamp} [${info.label}] ${info.message}`)
// )

const consoleFormat = format.combine(
  format.label({ label: loggerConfig.label }),
  format.colorize(),
  format.printf(info => `${info.level} [${info.label}] > ${info.message}`)
)

const logger = createLogger({
  levels: loggerConfig.levels,
  transports: [
    new transports.Console({
      level: loggerConfig.level,
      format: consoleFormat
    })
  ]
})
config.addColors(loggerConfig.colors)

module.exports = logger
